#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rusty_bot;

extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

mod events;

use rocket::request::Request;
use rocket::response::{Responder, Response};
use rocket::http::Status;

// TODO: Remove me when https://github.com/SergioBenitez/Rocket/pull/358 is merged and released
#[derive(Debug, Clone, PartialEq)]
struct BadRequest<R>(pub Option<R>);

impl<'r, R: Responder<'r>> Responder<'r> for BadRequest<R> {
    fn respond_to(self, req: &Request) -> Result<Response<'r>, Status> {
        let mut build = Response::build();
        if let Some(responder) = self.0 {
            build.merge(responder.respond_to(req)?);
        }

        build.status(Status::BadRequest).ok()
    }
}

fn main() {
    rocket::ignite()
        .mount("/gitlab/events", routes![events::gitlab::all])
        .launch();
}
