use rusty_bot::gitlab::{MergeRequest, Branch};

#[derive(Serialize, Deserialize, Debug)]
struct UserDto {
    name: String,
    username: String,
    avatar_url: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct ObjectAttributesDto {
    id: i64,
    source_branch: String,
    source_project_id: i64,
    target_branch: String,
    target_project_id: i64,
    author_id: Option<i64>,
    assignee_id: Option<i64>,
    assignee: Option<UserDto>,
    title: String,
    state: String,
    merge_status: String,
    description: String,
    work_in_progress: bool,
    url: String,
    action: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MergeRequestEventDto {
    object_kind: Option<String>,
    user: Option<UserDto>,
    object_attributes: ObjectAttributesDto,
}

pub fn assemble_from(dto: MergeRequestEventDto) -> MergeRequest {
    let source = Branch {
        name: dto.object_attributes.source_branch,
        project: dto.object_attributes.source_project_id,
    };
    let target = Branch {
        name: dto.object_attributes.target_branch,
        project: dto.object_attributes.target_project_id,
    };

    MergeRequest {
        id: dto.object_attributes.id,
        title: dto.object_attributes.title,
        source_branch: source,
        target_branch: target,
    }
}
