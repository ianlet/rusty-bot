mod merge_request;

use BadRequest;
use events::gitlab::merge_request::MergeRequestEventDto;

use rocket::response::status::NoContent;
use rocket::{Data, Outcome};
use rocket::data;
use rocket::data::FromData;
use rocket::http::Status;
use rocket::request;
use rocket::request::Request;
use rocket::request::FromRequest;

use serde_json;

use std::io::prelude::*;

#[derive(Debug, PartialEq, Deserialize)]
pub struct Payload(pub String);

const X_GITLAB_EVENT: &'static str = "X-Gitlab-Event";

const MERGE_REQUEST_EVENT: &'static str = "Merge Request Hook";

#[derive(Clone, Debug, PartialEq)]
enum HookEvent {
    MergeRequest,
}

impl<'r, 'a> FromRequest<'r, 'a> for HookEvent {
    type Error = ();

    fn from_request(request: &'r Request<'a>) -> request::Outcome<HookEvent, ()> {
        let keys = request.headers().get(X_GITLAB_EVENT).collect::<Vec<_>>();
        if keys.len() != 1 {
            return Outcome::Failure((Status::BadRequest, ()));
        }

        let event = match keys[0] {
            MERGE_REQUEST_EVENT => HookEvent::MergeRequest,
            _ => {
                return Outcome::Failure((Status::BadRequest, ()));
            }
        };

        Outcome::Success(event)
    }
}

impl FromData for Payload {
    type Error = ();

    fn from_data(request: &Request, data: Data) -> data::Outcome<Self, Self::Error> {
        let mut body = String::new();
        if let Err(_) = data.open().read_to_string(&mut body) {
            return Outcome::Failure((Status::InternalServerError, ()));
        }

        // TODO: Handle secrets

        Outcome::Success(Payload(body))
    }
}

#[post("/", format = "application/json", data = "<payload>")]
fn all(event: Option<HookEvent>, payload: Payload) -> Result<NoContent, BadRequest<String>> {
    match event {
        Some(HookEvent::MergeRequest) => {
            handle_merge_request(payload);
            Ok(NoContent)
        }
        _ => Err(BadRequest(Some(String::from("Unsupported hook event")))),
    }
}

fn handle_merge_request(payload: Payload) {
    let dto: MergeRequestEventDto = serde_json::from_str(&payload.0).unwrap();
    let merge_request_event = merge_request::assemble_from(dto);
}
