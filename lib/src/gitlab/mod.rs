pub struct Branch {
    pub name: String,
    pub project: i64,
}

pub struct MergeRequest {
    pub id: i64,
    pub title: String,
    pub source_branch: Branch,
    pub target_branch: Branch,
}
